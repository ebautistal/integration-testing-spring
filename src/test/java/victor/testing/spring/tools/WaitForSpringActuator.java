package victor.testing.spring.tools;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WaitForSpringActuator implements BeforeAllCallback {

    private final String baseUrl;

    public WaitForSpringActuator(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public void beforeAll(ExtensionContext context) {
        log.info("Waiting for {} to come UP...", baseUrl);
        Awaitility.await().pollInterval(1,TimeUnit.SECONDS)
            .atMost(1, TimeUnit.MINUTES)
            .until(() -> isApplicationUp(baseUrl + "/actuator"));
        log.info("{} is UP", baseUrl);
    }

    public static boolean isApplicationUp(String urlString) {
        try {
            log.debug("Ping {}", urlString);

//            var response = HttpClient.newHttpClient()
//                .send(HttpRequest.newBuilder().GET().uri(URI.create(urlString)).build(), BodyHandlers.ofString());
            
            RestTemplate restTemplate = new RestTemplate();
            RequestEntity<String> request = new RequestEntity<>(null, new HttpHeaders(), HttpMethod.GET, URI.create(urlString));
            ResponseEntity<String> re = restTemplate.exchange(URI.create(urlString), HttpMethod.GET, request, String.class);
            return re.getStatusCode().value() == 200;
            
//            return response.statusCode() == 200;
        } catch (Exception ex) {
            // ignore
            return false;
        }
    }
}
