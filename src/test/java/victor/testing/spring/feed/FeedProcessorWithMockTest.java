package victor.testing.spring.feed;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import victor.testing.spring.tools.MeasureTotalTestTimeListener.MeasureRealTime;

@SpringBootTest
@MeasureRealTime
public class FeedProcessorWithMockTest {

   @Autowired
   private FeedProcessor feedProcessor;
   @MockBean
   private IFileRepo fileRepoMock;


   @Test
   public void oneFileWithOneLine() {
	  List<String> listFiles = new ArrayList<>();
	  listFiles.add("one.txt");
		  
      when(fileRepoMock.getFileNames()).thenReturn(listFiles);
      when(fileRepoMock.openFile("one.txt")).thenReturn(Stream.of("one"));
      assertThat(feedProcessor.countPendingLines()).isEqualTo(1);
   }

   @Test
   public void oneFileWith2Lines() {
	  List<String> listFiles = new ArrayList<>();
	  listFiles.add("two.txt");
		  
      when(fileRepoMock.getFileNames()).thenReturn(listFiles);
      when(fileRepoMock.openFile("two.txt")).thenReturn(Stream.of("one","two"));
      assertThat(feedProcessor.countPendingLines()).isEqualTo(2);
   }

   @Test
   public void twoFilesWith3Lines() {
	  List<String> listFiles = new ArrayList<>();
	  listFiles.add("one.txt");
	  listFiles.add("two.txt");
		  
      when(fileRepoMock.getFileNames()).thenReturn(listFiles);
      when(fileRepoMock.openFile("one.txt")).thenReturn(Stream.of("one"));
      when(fileRepoMock.openFile("two.txt")).thenReturn(Stream.of("one","two"));
      assertThat(feedProcessor.countPendingLines()).isEqualTo(3);
   }

   @Test
   public void doesNotCountHashedLines() {
	  List<String> listFiles = new ArrayList<>();
	  listFiles.add("one.txt");
		  
      when(fileRepoMock.getFileNames()).thenReturn(listFiles);
      when(fileRepoMock.openFile("one.txt")).thenReturn(Stream.of("#one"));
      assertThat(feedProcessor.countPendingLines()).isEqualTo(0);
   }

   // TODO IMAGINE EXTRA DEPENDENCY
}
