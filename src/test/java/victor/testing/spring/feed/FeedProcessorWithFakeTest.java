package victor.testing.spring.feed;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import victor.testing.spring.tools.MeasureTotalTestTimeListener.MeasureRealTime;

@SpringBootTest
@MeasureRealTime
@ActiveProfiles({"fakeFileRepo"})
public class FeedProcessorWithFakeTest {

   @Autowired
   private FeedProcessor feedProcessor;
   @Autowired
   private FileRepoFake fileRepoFake;

   @BeforeEach
   public void cleanup() {
      fileRepoFake.clearFiles();
   }

   @Test
   public void oneFileWithOneLine() {
	  List<String> listLines = new ArrayList<>();
	  listLines.add("one.txt");
	  
      fileRepoFake.addFile("one.txt", listLines);
      assertThat(feedProcessor.countPendingLines()).isEqualTo(1);
   }

   @Test
//   @DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
   public void oneFileWith2Lines() {
	  List<String> listLines = new ArrayList<>();
	  listLines.add("one");
	  listLines.add("two");
		  
      fileRepoFake.addFile("two.txt", listLines);
      assertThat(feedProcessor.countPendingLines()).isEqualTo(2);
   }

   @Test
   public void twoFilesWith3Lines() {
	   
	  List<String> listLines1 = new ArrayList<>();
	  listLines1.add("one");
	  
	  List<String> listLines2 = new ArrayList<>();
	  listLines2.add("one");
	  listLines2.add("two");
		  
      fileRepoFake.addFile("one.txt", listLines1);
      fileRepoFake.addFile("two.txt", listLines2);
      assertThat(feedProcessor.countPendingLines()).isEqualTo(3);
   }

   @Test
   public void doesNotCountHashedLines() {
	  List<String> listLines = new ArrayList<>();
	  listLines.add("#one");
		  
      fileRepoFake.addFile("one.txt", listLines);
      assertThat(feedProcessor.countPendingLines()).isEqualTo(0);
   }
}
